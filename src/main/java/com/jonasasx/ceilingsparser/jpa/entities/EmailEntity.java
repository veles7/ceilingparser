package com.jonasasx.ceilingsparser.jpa.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

/**
 * @author Ivan Volynkin
 *         ivolynkin@roox.ru
 */
@NoArgsConstructor
@Data
@Entity
public class EmailEntity extends GenericEntity {

    private String url;

    public EmailEntity(String email) {
        super(email);
    }

    public EmailEntity(String email, String url) {
        super(email);
        this.url = url;
    }
}