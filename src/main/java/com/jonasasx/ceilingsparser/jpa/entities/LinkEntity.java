package com.jonasasx.ceilingsparser.jpa.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

/**
 * @author Ivan Volynkin
 *         ivolynkin@roox.ru
 */
@NoArgsConstructor
@Data
@Entity
public class LinkEntity extends GenericEntity {

    public LinkEntity(String value) {
        super(value);
    }
}