package com.jonasasx.ceilingsparser.jpa.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.util.Date;

/**
 * @author Ivan Volynkin
 *         ivolynkin@roox.ru
 */
@NoArgsConstructor
@Data
@MappedSuperclass
abstract public class GenericEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String value;
    private Date time;

    public GenericEntity(String value) {
        this.value = value;
    }

    @PrePersist
    public void onCreate() {
        time = new Date();
    }
}