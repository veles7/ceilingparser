package com.jonasasx.ceilingsparser.jpa.repositories;

import com.jonasasx.ceilingsparser.jpa.entities.GenericEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Ivan Volynkin
 *         ivolynkin@roox.ru
 */
public interface ValuedRepository<T extends GenericEntity> extends JpaRepository<T, String> {
    T findByValue(String value);
}
