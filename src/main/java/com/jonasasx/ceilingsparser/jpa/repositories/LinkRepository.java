package com.jonasasx.ceilingsparser.jpa.repositories;

import com.jonasasx.ceilingsparser.jpa.entities.LinkEntity;

/**
 * @author Ivan Volynkin
 *         ivolynkin@roox.ru
 */
public interface LinkRepository extends ValuedRepository<LinkEntity> {
}