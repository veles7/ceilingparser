package com.jonasasx.ceilingsparser.jpa.repositories;

import com.jonasasx.ceilingsparser.jpa.entities.EmailEntity;

/**
 * @author Ivan Volynkin
 *         ivolynkin@roox.ru
 */
public interface EmailRepository extends ValuedRepository<EmailEntity> {
}