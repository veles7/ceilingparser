package com.jonasasx.ceilingsparser.io;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * @author Ivan Volynkin
 *         jonasasx@gmail.com
 */
@Slf4j
public class XmlFixedHttpInputMessage implements HttpInputMessage {
    public static final Charset CHARSET = Charset.forName("UTF-8");
    private final HttpInputMessage message;

    public XmlFixedHttpInputMessage(HttpInputMessage message) {
        this.message = message;
    }

    @Override
    public InputStream getBody() throws IOException {
        String s = IOUtils.toString(message.getBody(), CHARSET);
        s = s.replaceAll("(?s)<(passage|title|headline)>(.*?)</\\1>", "<$1><![CDATA[$2]]></$1>");
        return new ByteArrayInputStream(s.getBytes(CHARSET));
    }

    @Override
    public HttpHeaders getHeaders() {
        return message.getHeaders();
    }
}