package com.jonasasx.ceilingsparser.services;

import com.jonasasx.ceilingsparser.jpa.entities.GenericEntity;
import com.jonasasx.ceilingsparser.jpa.repositories.ValuedRepository;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Function;

/**
 * @author Ivan Volynkin
 *         ivolynkin@roox.ru
 */
@Slf4j
abstract public class AbstractStorage<T extends GenericEntity> {

    private final Function<String, T> entityFactory;

    protected AbstractStorage(Function<String, T> entityFactory) {
        this.entityFactory = entityFactory;
    }

    public void addValue(String value) {
        addValue(entityFactory.apply(value));
    }

    public void addValue(T entity) {
        if (!isExists(entity.getValue())) {
            log.info("Add value: {}", entity);
            getRepository().save(entity);
        } else {
            log.info("Value exists: {}", entity);
        }
    }

    public boolean isExists(String value) {
        return getRepository().findByValue(value) != null;
    }

    abstract protected ValuedRepository<T> getRepository();

}