package com.jonasasx.ceilingsparser.services;

import com.google.common.util.concurrent.SimpleTimeLimiter;
import com.google.common.util.concurrent.TimeLimiter;
import com.google.common.util.concurrent.UncheckedTimeoutException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Ivan Volynkin
 *         jonasasx@gmail.com
 */
@Slf4j
@Component
public class SitesParser {
    private static final Pattern EMAIL_REGEX = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}");
    private static final Pattern CONTACTS_REGEX = Pattern.compile("<a[^>]+href=\"([^\"]+)\"[^>]*>.*?контакты.*?</a>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.UNICODE_CASE);


    private final CloseableHttpClient client;
    private final EmailsStorage emailsStorage;
    private final LinksStorage linksStorage;
    private final TimeLimiter timeLimiter = SimpleTimeLimiter.create(Executors.newCachedThreadPool());
    private final RegexFinder timeLimitedProxy;

    @Autowired
    public SitesParser(CloseableHttpClient client, EmailsStorage emailsStorage, LinksStorage linksStorage, @Value("${com.jonasasx.ceilingsparser.task.timeout:5000}") int timeout) {
        this.client = client;
        this.emailsStorage = emailsStorage;
        this.linksStorage = linksStorage;
        timeLimitedProxy = timeLimiter.newProxy(new RegexFinderImpl(this), RegexFinder.class, timeout, TimeUnit.MILLISECONDS);
    }

    public void parse(URI uri) {
        try {
            download(uri);
        } catch (Exception e) {
            log.error("Error on parsing {}", uri, e);
        }
    }

    private void download(URI uri) {
        if (linksStorage.isExists(uri.toString())) {
            return;
        }
        HttpUriRequest request = new HttpGet(uri);
        log.info("Getting {}", uri);
        try (CloseableHttpResponse response = client.execute(request)) {
            linksStorage.addValue(uri.toString());
            int code = response.getStatusLine().getStatusCode();
            if (code != 200) {
                log.info("Complete {}: code = {}", uri, code);
                return;
            }
            HttpEntity entity = response.getEntity();
            ContentType contentType = ContentType.getOrDefault(entity);
            Charset charset = contentType.getCharset();
            doParse(IOUtils.toString(response.getEntity().getContent(), charset), uri);
        } catch (IOException e) {
            log.warn("Error on getting {}: {}", uri, e);
        }
    }

    private void doParse(String html, URI baseUri) {
        try {
            timeLimitedProxy.findEmails(html, baseUri);
        } catch (UncheckedTimeoutException e) {
            log.warn("Timeout exceeded on emails");
        }
        try {
            timeLimitedProxy.findContacts(html, baseUri);
        } catch (UncheckedTimeoutException e) {
            log.warn("Timeout exceeded on contacts");
        }
    }

    static class RegexFinderImpl implements RegexFinder {
        private final SitesParser sitesParser;

        RegexFinderImpl(SitesParser sitesParser) {
            this.sitesParser = sitesParser;
        }

        @Override
        public void findEmails(String html, URI uri) {
            Matcher matcher = EMAIL_REGEX.matcher(html);
            while (matcher.find()) {
                String email = matcher.group();
                sitesParser.emailsStorage.addValue(email, uri.toString());
            }
        }

        @Override
        public void findContacts(String html, URI baseUri) {
            Matcher matcher = CONTACTS_REGEX.matcher(html);
            while (matcher.find()) {
                String url = matcher.group(1);
                try {
                    sitesParser.parse(new URI(baseUri.toString()).resolve(url));
                } catch (URISyntaxException e) {
                    log.warn("Illegal uri: {}", url);
                }
            }
        }
    }

    public interface RegexFinder {
        void findEmails(String html, URI uri);

        void findContacts(String html, URI baseUri);
    }
}