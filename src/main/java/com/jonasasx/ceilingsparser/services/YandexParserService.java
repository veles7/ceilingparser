package com.jonasasx.ceilingsparser.services;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * @author Ivan Volynkin
 *         jonasasx@gmail.com
 */
@Slf4j
@Service
public class YandexParserService {
    private final RestTemplate restTemplate;
    private final String endpoint;

    @Autowired
    public YandexParserService(RestTemplate restTemplate, @Value("${com.jonasasx.ceilingsparser.ya.endpoint:http://localhost:2009/search/xml}") String endpoint) {
        this.restTemplate = restTemplate;
        this.endpoint = endpoint;
    }

    public void parse(String query, Consumer<URI> consumer, int fromPage, int toPage) {
        int page = fromPage;
        while (page < toPage) {
            Request request = new Request(query, page);
            ResponseEntity<YandexSearch> response = null;
            while (response == null) {
                try {
                    response = restTemplate.postForEntity(endpoint, request, YandexSearch.class);
                } catch (Exception e) {
                    log.error("Error on YA request", e);
                }
            }
            if (response.getStatusCode().is2xxSuccessful()) {
                log.info("Success YA request page {}", page);
                Objects.requireNonNull(response.getBody()).getResponse().getResults().getGrouping().getGroups().stream()
                        .map(v -> v.getDoc().getUrl().trim())
                        .map(this::toUri)
                        .filter(Objects::nonNull)
                        .forEach(consumer);
            } else {
                log.error("Error YA: {}; {}", response.getStatusCode(), response);
            }
            page++;
        }
        log.info("Finish");
    }

    private URI toUri(String s) {
        try {
            return URI.create(s);
        } catch (Exception e) {
            log.warn("Illegal URI", e);
        }
        return null;
    }

    @JacksonXmlRootElement(localName = "request")
    @AllArgsConstructor
    @Data
    public static class Request {
        private String query;
        private int page;
    }

    @Data
    public static class YandexSearch {
        private Response response;
    }

    @Data
    public static class Response {
        private Results results;
    }

    @Data
    public static class Results {
        private Grouping grouping;
    }

    @Data
    public static class Grouping {
        @JacksonXmlElementWrapper(useWrapping = false)
        @JacksonXmlProperty(localName = "group")
        private List<Group> groups;
    }

    @Data
    public static class Group {
        private Doc doc;
    }

    @Data
    public static class Doc {
        private String url;
    }
}