package com.jonasasx.ceilingsparser.services;

import com.jonasasx.ceilingsparser.jpa.entities.EmailEntity;
import com.jonasasx.ceilingsparser.jpa.repositories.EmailRepository;
import com.jonasasx.ceilingsparser.jpa.repositories.ValuedRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Ivan Volynkin
 *         jonasasx@gmail.com
 */
@Slf4j
@Component
public class EmailsStorage extends AbstractStorage<EmailEntity> {
    private final EmailRepository emailRepository;

    @Autowired
    public EmailsStorage(EmailRepository emailRepository) {
        super(EmailEntity::new);
        this.emailRepository = emailRepository;
    }

    @Override
    protected ValuedRepository<EmailEntity> getRepository() {
        return emailRepository;
    }

    public void addValue(String email, String url) {
        addValue(new EmailEntity(email, url));
    }
}