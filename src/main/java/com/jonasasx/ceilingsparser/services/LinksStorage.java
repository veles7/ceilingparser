package com.jonasasx.ceilingsparser.services;

import com.jonasasx.ceilingsparser.jpa.entities.LinkEntity;
import com.jonasasx.ceilingsparser.jpa.repositories.LinkRepository;
import com.jonasasx.ceilingsparser.jpa.repositories.ValuedRepository;
import org.springframework.stereotype.Component;

/**
 * @author Ivan Volynkin
 *         ivolynkin@roox.ru
 */
@Component
public class LinksStorage extends AbstractStorage<LinkEntity> {

    private final LinkRepository linkRepository;

    public LinksStorage(LinkRepository linkRepository) {
        super(LinkEntity::new);
        this.linkRepository = linkRepository;
    }

    @Override
    protected ValuedRepository<LinkEntity> getRepository() {
        return linkRepository;
    }
}