package com.jonasasx.ceilingsparser;

import com.jonasasx.ceilingsparser.services.SitesParser;
import com.jonasasx.ceilingsparser.services.YandexParserService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class CeilingsparserApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext app = SpringApplication.run(CeilingsparserApplication.class, args);

        YandexParserService yandexParserService = app.getBean(YandexParserService.class);
        SitesParser sitesParser = app.getBean(SitesParser.class);
        int pageFrom = Integer.valueOf(app.getEnvironment().getProperty("com.jonasasx.ceilingsparser.page.from", "0"));
        int pageTo = Integer.valueOf(app.getEnvironment().getProperty("com.jonasasx.ceilingsparser.page.to", "30"));
        yandexParserService.parse("натяжные потолки спб", sitesParser::parse, pageFrom, pageTo);
        System.exit(0);
    }
}
