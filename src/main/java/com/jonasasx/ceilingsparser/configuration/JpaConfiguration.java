package com.jonasasx.ceilingsparser.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Ivan Volynkin
 *         ivolynkin@roox.ru
 */
@EnableJpaRepositories(value = "com.jonasasx.ceilingsparser.jpa.repositories", excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = ".*ValuedRepository.*"))
@EntityScan("com.jonasasx.ceilingsparser.jpa.entities")
@Configuration
public class JpaConfiguration {
}