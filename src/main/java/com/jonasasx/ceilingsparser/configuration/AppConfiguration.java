package com.jonasasx.ceilingsparser.configuration;

import com.jonasasx.ceilingsparser.io.XmlFixedHttpInputMessage;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collections;

/**
 * @author Ivan Volynkin
 *         jonasasx@gmail.com
 */
@Import({
        JpaConfiguration.class
})
@ComponentScan
@Configuration
public class AppConfiguration {


    @Bean
    public RestTemplate restTemplate(CloseableHttpClient httpClient) {
        RestTemplate restTemplate = new RestTemplate(Collections.singletonList(new MappingJackson2XmlHttpMessageConverter() {
            @Override
            protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage)
                    throws IOException, HttpMessageNotReadableException {
                return super.readInternal(clazz, new XmlFixedHttpInputMessage(inputMessage));
            }

            @Override
            public Object read(Type type, @Nullable Class<?> contextClass, HttpInputMessage inputMessage)
                    throws IOException, HttpMessageNotReadableException {
                return super.read(type, contextClass, new XmlFixedHttpInputMessage(inputMessage));
            }
        }));
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
        return restTemplate;
    }

    @Bean
    public CloseableHttpClient httpClient() {
        RequestConfig config = RequestConfig.custom()
                .setConnectionRequestTimeout(5000)
                .setConnectTimeout(5000)
                .build();
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(2000);
        cm.setDefaultMaxPerRoute(20);
        return HttpClientBuilder.create()
                .setConnectionManager(cm)
                .setDefaultRequestConfig(config)
                .build();
    }

}